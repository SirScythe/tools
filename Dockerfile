FROM registry.fedoraproject.org/fedora:latest

RUN dnf install -y jq  \
  	&& dnf clean all \
  	&& rm -rf /var/cache/yum
